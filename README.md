# fontconfig

Package fontconfig is a font configuration and customization library.

## Installation

    $ go get modernc.org/fontconfig

## Linking using ccgo

    $ ccgo foo.c -lmodernc.org/fontconfig/lib

## Documentation

[godoc.org/modernc.org/fontconfig](http://godoc.org/modernc.org/fontconfig)

## Builders

[modern-c.appspot.com/-/builder/?importpath=modernc.org%2ffontconfig](https://modern-c.appspot.com/-/builder/?importpath=modernc.org%2ffontconfig)
